# CSS Utils
Collection of mixins to help during development


```
npm i @fantassin/utils
```

## A11Y
* Colors
* Screen Reader

## Layout
* Spacings
* Line Widths
* Media Queries (breakpoints)
  
## Typography
* Headings
* Convert px to rem with rem-calc()

## Animations
* Easings

## Examples

### Colors

```
$colors: (
        'black': var(--black),
        'white': var(--white),
        'red': $red,
        'transparent': transparent,
);

$color-from-background-color: (
        'white': getColorFromName('black'),
        'black': var(--white),
        'red': $white,
);

.has-red-background-color {
    background-color: getColorFromName('red');
    color: getColorFromBackgroundColorName('red');
}
```
### Headings

```
$headings: (
	'h1': (
		'small': (24px, 30px),
		'large': (48px, 60px)
	),
	'h2': (
		'small': (20px, 27px),
		'large': (33px, 45px)
	),
	'h3': (
		'small': (18px, 24px),
		'large': (26px, 35px)
	),
	'h4': (
		'small': (16px, 22px),
		'large': (20px, 27px)
	),
	'h5': (
		'small': (14px, 19px),
		'large': (18px, 27px)
	),
	'h6': (
		'small': (12px, 16px),
		'large': (14px, 19px)
	),
);

h1 {
    @include getHn( 'h1' );
}

```

### Spacings

```
$spacings : (
    'cover': (
        'small': 20px,
        'large': 40px
    )
    'group': 60px
);

.wp-block-cover {
    getSpacing( 'margin-top', 'cover' );
    getSpacing( 'margin-bottom', 'cover' );
}

.wp-block-group {
    getSpacing( 'padding-top', 'group' );
    getSpacing( 'padding-bottom', 'group' );
}
```

### Line Widths

```
$line-widths : (
    'paragraph': 720px,
    'heading': 900px
);

h1, 
h2, 
h3, 
h4, 
h5, 
h6 {
    max-width: getLineWidthValue( 'heading' );
}

p, 
ol,
ul {
    max-width: getLineWidth( 'paragraph' ); // Alias of getLineWidthValue().
}

```

Made by [Fantassin](https://fantassin.fr)
